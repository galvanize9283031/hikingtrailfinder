
function getPlacesToHike(city, state) {
    const apiKey = 'AIzaSyB4ZcFLE9hUaRjNCWF_6Fa4v6zZPyqLz2U';
    const keyword = 'park for hiking'; // Your search keyword
    const radius = 10000; // Search within a 10 km radius
    const place = city + ", " + state;

    const apiUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${place}&radius=${radius}&keyword=${keyword}&key=${apiKey}`;

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            const results = data.results;
            for (const result of results) {
                console.log(`Name: ${result.name}`);
                console.log(`Address: ${result.vicinity}`);
                console.log(`Rating: ${result.rating || 'N/A'}`);
                console.log('\n');
            }
            return results;
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

/*
Just replace 'YOUR_API_KEY' with your actual Google API key, and specify the city, keyword, and radius as needed. This JavaScript code sends a request to the Google Places API and logs the results in the console. You can further customize and format the output as per your requirements.
*/

var result = getPlacesToHike("San Francisco", "CA");
console.log(result);
