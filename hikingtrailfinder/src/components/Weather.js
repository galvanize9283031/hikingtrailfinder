import React from 'react';


const OPEN_WEATHER_API_KEY = "";

async function get_weather_data(city, state) {
    // Use the Open Weather API
    // Get coordinates
    const place = city + ", " + state;
    const geoResponse = await fetch(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&appid=${OPEN_WEATHER_API_KEY}&limit=1`);
    const geoData = await geoResponse.json();
    if ('cod' in geoData) {
        // got an error, return empty data
        return null;
    }
    const { lat, lon } = geoData[0] || {};
    if (!lat || !lon) {
        // coordinates not found, return empty data
        return null;
    }
    // get weather for the given coordinates
    const weatherResponse = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${OPEN_WEATHER_API_KEY}&units=imperial`);
    const weatherData = await weatherResponse.json();
    if (weatherData.cod !== 200) {
        // got an error, return empty data
        return null;
    }
    const { temp } = weatherData.main;
    const { main: description } = weatherData.weather[0];
    console.log(temp, description);
    return { temp, description };
}


/**
 *
 * @param {*} city
 * @param {*} state
 * @returns
 *
 */
function get_weather(city, state) {
    return get_weather_data(city, state).then(data => {
        console.log(data);
        return data;
    });
}

// get_weather("San Francisco", "CA");
// console.log(get_weather("San Francisco", "CA"));

/**
 *
 * @param {*} props.city - city name
 * @param {*} props.state - state abbreviation
 *
 * @returns
 */
function Weather(props) {
  return (
    <div className='weather-container'>
        <h2>Weather</h2>
        <div className='weather-data'>
            <p>Temperature: {get_weather("San Francisco", "CA")}</p>
            <p>Weather: {get_weather("San Francisco", "CA")}</p>
        </div>
    </div>
  );
}

export default Weather;
